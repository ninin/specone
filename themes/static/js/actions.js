$(document).ready(function () {
    var like_button = $('.btn-like');
    var remove_button = $('.btn-remove');

    function remove_movie(id) {
        var data = {
            action: 'delete',
            id: id,
        }
        $.ajax({
            type: 'POST',
            url: '/movie/ajax/',
            data: data,
            success:function(data){
            },
            error:function(data){
            }
        });
    }

    function like_movie(id) {
        var data = {
            action: 'like',
            id: id,
        }
        $.ajax({
            type: 'POST',
            url: '/movie/ajax/',
            data: data,
            success:function(data){
            },
            error:function(data){
            }
        });
    }

    like_button.on('click', function(e){
        e.preventDefault();
        movie_id = parseInt($(this).closest('tr,div').find('input:hidden').val());
        if ($(this).hasClass('btn-white')){
            $(this).removeClass('btn-white').addClass('btn-primary');
            like_movie(movie_id);
        } else {
            $(this).removeClass('btn-primary').addClass('btn-white');
            like_movie(movie_id);
        }
    });

    remove_button.on('click', function(e){
        e.preventDefault();
        movie_id = parseInt($(this).closest('tr').find('input:hidden').val());
        swal({
            title: "Are you sure?",
            text: "You will not be able to see this movie in the list anymore!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            $("tr.movie-"+ movie_id).addClass('animated slideOutRight').slideUp();
            remove_movie(movie_id);
            swal({
                title: "Deleted!",
                text: "Movie id " + movie_id + " has been deleted.",
                type: "success",
                showConfirmButton: false,
                timer: 1500
            });
        });
    });
});