# -*- coding: utf-8 -*-
import json
from django.contrib import auth, messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import (
    Paginator,
    EmptyPage,
    InvalidPage,
)
from django.core.urlresolvers import reverse
from django.http import (
    HttpResponse,
    HttpResponseBadRequest,
    HttpResponseRedirect
)
from django.shortcuts import render, get_object_or_404
from django.utils.decorators import method_decorator
from django.views import View
from specone.mixins import AJAXOnlyMixin, JSONResponseMixin
from specone.models import Movie
from specone.decorators import anonymous_required
from specone.forms import (
    LoginForm,
    MovieForm,
    RegisterForm,
)


class LoginView(View):

    login_template = 'login_landscape.html'

    @method_decorator(anonymous_required)
    def get(self, request, *args, **kwargs):
        form = LoginForm()
        context = {
            'form': form,
        }
        return render(request, self.login_template, context)

    def post(self, request, *args, **kwargs):
        form = LoginForm(request.POST)
        if form.is_valid():
            user = form.submit()
            auth.login(request, user)
            return HttpResponseRedirect('/')
        context = {
            'form': form,
        }
        return render(request, self.login_template, context)


@login_required
def logout(request):
    auth.logout(request)
    return HttpResponseRedirect(reverse('login'))


class RegisterView(View):

    register_template = 'register_landscape.html'

    @method_decorator(anonymous_required)
    def get(self, request, *args, **kwargs):
        form = RegisterForm()
        context = {
            'form': form,
        }
        return render(request, self.register_template, context)

    def post(self, request, *args, **kwargs):
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            if user:
                auth.login(request, user)
                return HttpResponseRedirect('/')
        context = {
            'form': form,
        }
        return render(request, self.register_template, context)


class MovieListView(View):

    movie_list_template = 'movie_list.html'

    def get(self, request, *args, **kwargs):
        movie_items = Movie.objects.filter(is_active=True).order_by('title')
        page = int(request.GET.get('page', 1))

        paginator = Paginator(movie_items, 20)

        try:
            movie_items = paginator.page(page)
        except (EmptyPage, InvalidPage):
            page = paginator.num_pages
            movie_items = paginator.page(page)

        counter_modifier = 20 * (page - 1)

        context = {
            'items': movie_items,
            'page': page,
            'counter_modifier': counter_modifier,
        }
        return render(request, self.movie_list_template, context)


class MovieDetailsView(View):

    move_details_template = 'movie_details.html'

    def get(self, request, *args, **kwargs):
        pid = kwargs.get('pid', None)
        movie = get_object_or_404(Movie, pk=pid)
        context = {
            'item': movie
        }
        return render(request, self.move_details_template, context)


class MovieCreateView(View):

    movie_create_template = 'movie_create.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(MovieCreateView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        form = MovieForm()
        context = {
            'form': form
        }
        return render(request, self.movie_create_template, context)

    def post(self, request, *args, **kwargs):
        form = MovieForm(request.POST)
        movie = None
        if form.is_valid():
            movie = form.save(request.user)
            messages.success(request, '{} successfully created!!.'.format(movie.title))
            return HttpResponseRedirect(reverse('movie_detail', kwargs={'pid': movie.pk}))
        return render(request, self.movie_create_template, {'form': form})


class MovieEditView(View):

    movie_edit_template = 'movie_edit.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(MovieEditView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        pid = kwargs.get('pid', None)
        movie = get_object_or_404(Movie, pk=pid)
        form = MovieForm(obj=movie)
        context = {
            'form': form,
            'item': movie
        }
        return render(request, self.movie_edit_template, context)

    def post(self, request, *args, **kwargs):
        pid = kwargs.get('pid', None)
        movie = get_object_or_404(Movie, pk=pid)
        form = MovieForm(request.POST, obj=movie)
        if form.is_valid():
            movie = form.save(request.user)
            messages.success(request, '{} successfully edited!!.'.format(movie.title))
            return HttpResponseRedirect(reverse('movie_detail', kwargs={'pid': movie.pk}))
        return render(request, self.movie_edit_template, {'form': form})


class MovieAjaxView(AJAXOnlyMixin, JSONResponseMixin, View):

    def post(self, request, *args, **kwargs):
        msg = ""
        success = True
        movie_obj = None
        movie_id = request.POST.get('id', None)
        try:
            movie_obj = Movie.objects.get(id=movie_id, is_active=True)
        except Movie.DoesNotExist:
            success = False
            msg = "Movie DoesNotExist!!"

        action = request.POST.get('action', None)
        if success and movie_obj and action in ['like', 'delete']:
            if action == 'like':
                movie_obj.is_like = not movie_obj.is_like
            elif action == 'delete':
                movie_obj.is_active = False
            movie_obj.save()

        json_data = json.dumps({
            "success": success,
            "msg": msg
        })
        if not success:
            return HttpResponseBadRequest(json_data, content_type='application/json')
        return HttpResponse(json_data, content_type="application/json")
