from django import template

register = template.Library()


@register.assignment_tag
def check_last_visit(request):
    try:
        if 'history' in request.session and request.user.is_anonymous():
            request_path = request.get_full_path()
            history = request.session['history']
            if request_path in history:
                return history.get(request_path).strftime("%Y-%m-%d %H:%M:%S")
    except Exception:
        pass
    return ''


@register.inclusion_tag('pagination.html', takes_context=True)
def paginator(context, paginator_obj, padding=5, extra_params=None, custom_params=None, custom_page=None):
    pager = paginator_obj.paginator
    if custom_page:
        current = custom_page
    else:
        current = context['page']

    start_middle_page = None
    start_page = max(current - padding, 1)
    if start_page <= padding + 1:
        start_page = 1
    else:
        start_middle_page = abs(start_page / 2) + 1

    end_middle_page = None
    end_page = current + padding + 1
    if end_page >= pager.num_pages - 1:
        end_page = pager.num_pages + 1
    else:
        nums = range(end_page, pager.num_pages)
        length = len(nums)
        end_middle_page = abs(nums[(length - 1) / 2]) + 1

    page_numbers = [
        n for n in range(start_page, end_page) \
        if n > 0 and n <= pager.num_pages
    ]

    return {
        'show_first': 1 not in page_numbers,
        'has_previous': paginator_obj.has_previous,
        'previous': current - 1,
        'first_page': 1,
        'current_page': current,
        'page_numbers': page_numbers,
        'last_page': pager.num_pages,
        'has_next': paginator_obj.has_next,
        'next': current + 1,
        'show_last': pager.num_pages not in page_numbers,
        'extra_params': extra_params,
        'custom_params': custom_params if custom_params else 'page',
        'end_middle_page': end_middle_page,
        'start_middle_page': start_middle_page
    }
