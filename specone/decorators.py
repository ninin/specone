import functools

from django.conf import settings
from django.http import HttpResponseRedirect


def anonymous_required(view_func):
    def _decorator(request, *args, **kwargs):
        if request.user is not None and request.user.is_authenticated():
            return HttpResponseRedirect(settings.LOGIN_REDIRECT_URL)
        else:
            response = view_func(request, *args, **kwargs)
            return response
    return functools.wraps(view_func)(_decorator)
