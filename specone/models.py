# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime
from django.conf import settings
from django.db import models


class Movie(models.Model):
    title = models.CharField(max_length=255)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    description = models.TextField(blank=True)
    is_active = models.BooleanField(default=True)
    is_like = models.BooleanField(default=False)
    updated = models.DateTimeField(blank=True, null=True)
    created = models.DateTimeField(blank=True, auto_now_add=True)

    def save(self, *args, **kwargs):
        # On save, update timestamps
        self.updated = datetime.now()
        super(Movie, self).save(*args, **kwargs)
