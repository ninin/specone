import functools
import json

from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseBadRequest


def ajax_required(fn):
    """
    AJAX request required decorator
    use it in your views:

    @ajax_required
    def my_view(request):
        ....
    """
    @functools.wraps(fn)
    def wrapper(request, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponseBadRequest()
        return fn(request, *args, **kwargs)

    return wrapper


class JSONResponseMixin(object):

    """
    A mixin that can be used to render a JSON response.
    """
    response_class = HttpResponse

    def render_to_json_response(self, context, **response_kwargs):
        """
        Returns a JSON response, transforming 'context' to make the payload.
        """
        response_kwargs['content_type'] = 'application/json'
        return self.response_class(
            self.convert_context_to_json(context),
            **response_kwargs
        )

    def convert_context_to_json(self, context):
        "Convert the context dictionary into a JSON object"
        # Note: This is *EXTREMELY* naive; in reality, you'll need
        # to do much more complex handling to ensure that arbitrary
        # objects -- such as Django model instances or querysets
        # -- can be serialized as JSON.
        return json.dumps(context)


class AJAXOnlyMixin(object):

    """
    A mixin for decorating a View's dispatch method with
    a ajax_required decorator
    """

    @method_decorator(ajax_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(AJAXOnlyMixin, self).dispatch(*args, **kwargs)
