from datetime import datetime


class UserLastVisited(object):

    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        response = self.get_response(request)

        if request.user.is_anonymous():
            request_path = request.get_full_path()

            if 'history' not in request.session:
                request.session['history'] = {request_path: datetime.now()}
            else:
                history = request.session['history']
                history.update({
                    request_path: datetime.now()
                })
                request.session['history'] = history
            request.session.save()

        # Code to be executed for each request/response after
        # the view is called.

        return response
