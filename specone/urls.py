"""specone URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from specone.views import (
    logout,
    LoginView,
    MovieAjaxView,
    MovieCreateView,
    MovieDetailsView,
    MovieEditView,
    MovieListView,
    RegisterView,
)

urlpatterns = [
    url(r'^$', MovieListView.as_view(), name='index'),
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^logout/$', logout, name='logout'),
    url(r'^register/$', RegisterView.as_view(), name='register'),
    url(r'^create$', MovieCreateView.as_view(), name='movie_create'),
    url(r'^(?P<pid>[\d-]+)/edit$', MovieEditView.as_view(), name='movie_edit'),
    url(r'^(?P<pid>[\d-]+)', MovieDetailsView.as_view(), name='movie_detail'),
    url(r'^movie/ajax/$', MovieAjaxView.as_view(), name='movie_ajax'),
    url(r'^admin/', admin.site.urls),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
