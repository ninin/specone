from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.forms.widgets import PasswordInput, EmailInput
from django_password_strength.widgets import (
    PasswordStrengthInput,
    PasswordConfirmationInput)
from specone.models import Movie


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=PasswordInput())

    def clean(self):
        username = self.cleaned_data['username']
        password = self.cleaned_data['password']
        self.user = authenticate(username=username, password=password)
        if not self.user:
            raise forms.ValidationError("Authenticaation Failed")
        return self.cleaned_data

    def submit(self):
        return self.user


class RegisterForm(forms.Form):
    MIN_LENGTH = 8
    username = forms.CharField()
    email = forms.CharField(widget=EmailInput())
    password = forms.CharField(widget=PasswordStrengthInput())
    repassword = forms.CharField(widget=PasswordConfirmationInput())

    def clean_password(self):
        password = self.cleaned_data.get('password')
        # At least MIN_LENGTH long
        if len(password) < self.MIN_LENGTH:
            raise forms.ValidationError("Password must be at least %d characters long." % self.MIN_LENGTH)
        return password

    def save(self):
        user = None
        try:
            user = User.objects.create_user(
                username=self.cleaned_data.get('username'),
                email=self.cleaned_data.get('email'),
                password=self.cleaned_data.get('password'))
        except Exception:
            pass
        return user


class MovieForm(forms.Form):
    title = forms.CharField(
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Movie Title'}))
    description = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control'}),
        help_text='movie description')
    is_active = forms.BooleanField(
        required=False,
        initial=True,
        widget=forms.CheckboxInput(attrs={'class': 'form-control'}),
        help_text='if un-checked movie will not display in movie list.')
    is_like = forms.BooleanField(
        required=False,
        widget=forms.CheckboxInput(attrs={'class': 'form-control'}),
        help_text='if checked will mark movie as like.')

    def __init__(self, *args, **kwargs):
        self.obj = kwargs.pop('obj', None)
        super(MovieForm, self).__init__(*args, **kwargs)
        if self.obj:
            self.fields['title'].initial = self.obj.title
            self.fields['description'].initial = self.obj.description
            self.fields['is_active'].initial = self.obj.is_active
            self.fields['is_like'].initial = self.obj.is_like

    def save(self, user=None):
        try:
            if self.obj:
                is_changed = False
                if self.cleaned_data['title'] != self.obj.title:
                    self.obj.title = self.cleaned_data['title']
                    is_changed = True
                if self.cleaned_data['description'] != self.obj.description:
                    self.obj.description = self.cleaned_data['description']
                    is_changed = True
                if self.cleaned_data['is_active'] != self.obj.is_active:
                    self.obj.is_active = self.cleaned_data['is_active']
                    is_changed = True
                if self.cleaned_data['is_like'] != self.obj.is_like:
                    self.obj.is_like = self.cleaned_data['is_like']
                    is_changed = True
                if is_changed:
                    self.obj.save()
            else:
                self.obj = Movie.objects.create(author=user, **self.cleaned_data)
        except Exception:
            pass
        return self.obj
