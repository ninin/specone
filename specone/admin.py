# -*- coding: utf-8 -*-
from django.contrib import admin
from specone.models import Movie


class MovieAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'author', 'is_active', 'is_like', 'updated', 'created', )
    list_filter = ('is_active', 'is_like')


admin.site.register(Movie, MovieAdmin)
